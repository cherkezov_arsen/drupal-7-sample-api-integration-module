<?php

/**
 * @file
 * IQVIA ClearTrak Admin Config.
 */

/**
 * IQVIA ClearTrak Admin settings form.
 */
function incy_iqvia_cleartrak_admin_config() {
  $form = array();

  $form['cleartrak_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('IQVIA ClearTrak API Config'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['cleartrak_config']['iqvia_cleartrak_enable_api'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable IQVIA ClearTrak API Integration'),
    '#default_value' => variable_get('iqvia_cleartrak_enable_api', 0),
  );

  $form['cleartrak_config']['iqvia_cleartrak_enable_debug_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable IQVIA ClearTrak API Debug mode'),
    '#default_value' => variable_get('iqvia_cleartrak_enable_debug_mode', 0),
  );

  $form['cleartrak_config']['iqvia_cleartrak_site_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Domain Name'),
    '#size' => 100,
    '#default_value' => variable_get('iqvia_cleartrak_site_domain', ''),
    '#required' => TRUE,
  );

  $form['cleartrak_config']['iqvia_cleartrak_api_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('IQVIA ClearTrak API Endpoint'),
    '#size' => 100,
    '#default_value' => variable_get('iqvia_cleartrak_api_endpoint', 'https://incyte-cleartrak.solutions.iqvia.com'),
    '#required' => TRUE,
  );

  $form['cleartrak_config']['iqvia_cleartrak_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('IQVIA ClearTrak API Username'),
    '#size' => 100,
    '#default_value' => variable_get('iqvia_cleartrak_api_username', ''),
    '#required' => TRUE,
  );

  $form['cleartrak_config']['iqvia_cleartrak_api_password'] = array(
    '#type' => 'textfield',
    '#title' => t('IQVIA ClearTrak API Password'),
    '#size' => 100,
    '#default_value' => variable_get('iqvia_cleartrak_api_password', ''),
    '#required' => TRUE,
  );

  $form['iqvia_cleartrak_deployment'] = array(
    '#type' => 'fieldset',
    '#title' => t('IQVIA ClearTrak API Push all Data to the API'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['iqvia_cleartrak_deployment']['confirm_data_push'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable this option to be able to use th e"Push All registrations to the API Now"'),
    '#default_value' => 0,
  );

  $form['iqvia_cleartrak_deployment']['push_all_registrations'] = array(
    '#type' => 'button',
    '#value' => 'Push All registrations to the API Now',
    '#ajax' => array(
      'callback' => 'incy_iqvia_cleartrak_push_all_registrations',
      'wrapper' => 'iqvia-deployment-messages',
      'method' => 'html',
      'effect' => 'none',
    ),
  );

  $form['iqvia_deployment']['message'] = array(
    '#prefix' => t('<div id="iqvia-deployment-messages">'),
    '#suffix' => t('</div>'),
  );

  return system_settings_form($form);
}

/**
 * Push all registrations form button.
 */
function incy_iqvia_cleartrak_push_all_registrations($form, $form_state) {
  $markup = '';
  if (empty($form_state['values']['confirm_data_push'])) {
    form_set_error('incy_iqvia_admin_config_confirm_data_push', t('You must enable the Push All registrations to the API Now to trigger API Push .'));
  }
  else {
    incy_iqvia_cleartrak_push_all_registrations_to_iqvia_api();
    $markup .= '<span class="">' . t('The API call has been triggered.') . '</span>';
  }

  return $markup;
}
