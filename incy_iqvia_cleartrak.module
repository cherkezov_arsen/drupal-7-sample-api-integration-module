<?php

/**
 * @file
 * INCY IQVIA ClearTrak API Integration.
 */

define('INCY_IQVIA_CLEARTRAK_SITE_DOMAIN', variable_get('iqvia_cleartrak_site_domain', 'infocusmeeting.com'));
define('INCY_IQVIA_CLEARTRAK_ENABLE_API', variable_get('iqvia_cleartrak_enable_api', 0));
define('INCY_IQVIA_CLEARTRAK_ENABLE_DEBUG_MODE', variable_get('iqvia_cleartrak_enable_debug_mode', 1));

/**
 * Implements hook_menu().
 */
function incy_iqvia_cleartrak_menu() {
  $items['admin/config/mmc/cleartrak'] = array(
    'title' => 'IQVIA ClearTrak API config',
    'description' => 'Configuration for IQVIA ClearTrak API',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('incy_iqvia_cleartrak_admin_config'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'incy_iqvia_cleartrak.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_cronapi().
 */
function incy_iqvia_cleartrak_cronapi() {
  $items = array();

  if (!INCY_IQVIA_CLEARTRAK_ENABLE_API) {
    return $items;
  }

  $items['incy_iqvia_cleartrak_cron_optout'] = array(
    'title' => t('Update IQVIA ClearTrak Optout'),
    'callback' => 'incy_iqvia_cleartrak_update_consumer_optout',
    'enabled' => TRUE,
    'tags' => array('IQVIA ClearTrak'),
    'scheduler' => array(
      'name' => 'crontab',
      'crontab' => array(
        'rules' => array('*/5 0-7 * * *'),
      ),
    ),
    'launcher' => array(
      'name' => 'serial',
      'serial' => array(
        'thread' => 'any',
      ),
    ),
  );

  $items['incy_iqvia_cleartrak_cron_push_registrations'] = array(
    'title' => t('Update IQVIA ClearTrak Registrations'),
    'callback' => 'incy_iqvia_cleartrak_push_upcoming_registrations_to_iqvia_api',
    'enabled' => TRUE,
    'tags' => array('IQVIA ClearTrak'),
    'launcher' => array(
      'name' => 'serial',
      'serial' => array(
        'thread' => 'any',
      ),
    ),
  );

  return $items;
}

/**
 * Push All registrations for only Upcoming events to IQVIA ClearTrak API.
 */
function incy_iqvia_cleartrak_push_upcoming_registrations_to_iqvia_api() {
  $event_registrations = views_get_view_result('iqvia_cleartrak_registration_list', 'upcoming_events_registrations');
  foreach ($event_registrations as $key => $registration) {
    $registration_node = $registration->_field_data['nid']['entity'];
    incy_iqvia_cleartrak_push_registration_to_iqvia($registration_node);
  }
}

/**
 * Push registrations for Upcoming and Completed events to IQVIA ClearTrak API.
 */
function incy_iqvia_cleartrak_push_all_registrations_to_iqvia_api() {
  $event_registrations = views_get_view_result('iqvia_cleartrak_registration_list', 'registration_list');

  foreach ($event_registrations as $key => $registration) {
    $registration_node = $registration->_field_data['nid']['entity'];
    incy_iqvia_cleartrak_push_registration_to_iqvia($registration_node);
  }
}

/**
 * Push individual Registration Data to the API.
 */
function incy_iqvia_cleartrak_push_registration_to_iqvia($node) {
  if (INCY_IQVIA_CLEARTRAK_ENABLE_API) {
    $registration_data = incy_iqvia_cleartrak_get_registration_data($node);
    $token = incy_iqvia_cleartrak_obtain_token();

    if (!empty($token) && incy_iqvia_cleartrak_consumer_registration($registration_data, $token)) {
      $node->field_iqvia_api_sent[LANGUAGE_NONE][0]['value'] = 1;
      field_attach_update('node', $node);
      $event_id = $node->field_event[LANGUAGE_NONE][0]['target_id'];
      $log_api_calls = incy_iqvia_cleartrak_api_write_log($node->nid, $event_id);
    }
  }
}

/**
 * Prepare Registration data for the API Calls.
 */
function incy_iqvia_cleartrak_get_registration_data($node) {
  $registration_data = array();

  $registration_data['FirstName'] = !empty($node->field_first_name[LANGUAGE_NONE][0]['value']) ? $node->field_first_name[LANGUAGE_NONE][0]['value'] : '';
  $registration_data['LastName'] = !empty($node->field_last_name[LANGUAGE_NONE][0]['value']) ? $node->field_last_name[LANGUAGE_NONE][0]['value'] : '';
  $registration_data['DataSource'] = INCY_IQVIA_CLEARTRAK_SITE_DOMAIN;
  $trackingCode = incy_iqvia_cleartrak_get_tracking_code();
  $registration_data['TrackingCode'] = $trackingCode;

  $state_ids = incy_iqvia_cleartrak_state_ids_list();
  $state_code = !empty($node->field_attendee_address[LANGUAGE_NONE][0]['administrative_area']) ? $node->field_attendee_address[LANGUAGE_NONE][0]['administrative_area'] : 'XX';

  $phone_number = !empty($node->field_phone[LANGUAGE_NONE][0]['value']) ? $node->field_phone[LANGUAGE_NONE][0]['value'] : '';
  $phone_pattern = array('(', ') ');
  $phone_replacement = array('', '-');
  if (!empty($phone_number)) {
    $phone_number = str_replace($phone_pattern, $phone_replacement, $phone_number);
  }

  $registration_data['ConsumerLocations'] = [
    (object) [
      'EmailAddress' => !empty($node->field_email[LANGUAGE_NONE][0]['email']) ? $node->field_email[LANGUAGE_NONE][0]['email'] : '',
      'PhoneNumber' => $phone_number,
      'Address1' => !empty($node->field_attendee_address[LANGUAGE_NONE][0]['thoroughfare']) ? $node->field_attendee_address[LANGUAGE_NONE][0]['thoroughfare'] : '',
      'City' => !empty($node->field_attendee_address[LANGUAGE_NONE][0]['locality']) ? $node->field_attendee_address[LANGUAGE_NONE][0]['locality'] : '',
      'StateCodeId' => $state_ids[$state_code],
      'ZipCode' => !empty($node->field_attendee_address[LANGUAGE_NONE][0]['postal_code']) ? $node->field_attendee_address[LANGUAGE_NONE][0]['postal_code'] : '',
      'DataSource' => INCY_IQVIA_CLEARTRAK_SITE_DOMAIN,
    ],
  ];

  $event = node_load($node->field_event[LANGUAGE_NONE][0]['target_id']);
  $field_source_info = field_info_field('field_source');
  $field_source_values = &$field_source_info['settings']['allowed_values'];
  $field_consumer_type = '';
  if (!empty($node->field_attendee_type[LANGUAGE_NONE][0]['value'])) {
    switch ($node->field_attendee_type[LANGUAGE_NONE][0]['value']) {
      case 'patient':
        $field_consumer_type = 'Patient';
        break;

      case 'caregiver':
        $field_consumer_type = 'Caregiver';
        break;

      default:
        $field_consumer_type = '';
        break;
    }
  }

  $consumer_attributes = [
    (object) [
      'Code' => 'MCBU',
      'Type' => 'Consumer',
      'FieldName' => '100018727',
      'FieldValue' => 'OCEMarketing',
      'DataSource' => INCY_IQVIA_CLEARTRAK_SITE_DOMAIN,
    ],
  ];

  $attributes = [
    'Registrant Type' => $field_consumer_type,
    'I\'m most interested in learning about' => !empty($node->field_mpn_type[LANGUAGE_NONE][0]['value']) ? strtoupper($node->field_mpn_type[LANGUAGE_NONE][0]['value']) : '',
    'How did you hear about this event' => !empty($node->field_source[LANGUAGE_NONE][0]['value']) ? $field_source_values[$node->field_source[LANGUAGE_NONE][0]['value']] : '',
    'Number of Guests' => !empty($node->field_guest[LANGUAGE_NONE][0]['value']) ? $node->field_guest[LANGUAGE_NONE][0]['value'] : 0,
    'Are either you or your guest a practicing healthcare professional' => !empty($node->field_hcp[LANGUAGE_NONE][0]['value']) ? ucwords($node->field_hcp[LANGUAGE_NONE][0]['value']) : '',
  ];
  $external_event_id = incy_iqvia_cleartrak_get_external_id($event->nid);
  foreach ($attributes as $attr_name => $attr_value) {
    $consumer_attributes[] = (object) [
      'Code' => $external_event_id,
      'Type' => 'Patient Meeting Registration',
      'FieldName' => $attr_name,
      'FieldValue' => $attr_value,
      'DataSource' => INCY_IQVIA_CLEARTRAK_SITE_DOMAIN,
    ];
  }
  $registration_data['ConsumerAttributes'] = $consumer_attributes;

  $event_start_date = new DateTime($event->field_date[LANGUAGE_NONE][0]['value']);
  $event_start_date->setTimezone(new DateTimeZone($event->field_date[LANGUAGE_NONE][0]['timezone']));

  $event_end_date = new DateTime($event->field_date[LANGUAGE_NONE][0]['value2']);
  $event_end_date->setTimezone(new DateTimeZone($event->field_date[LANGUAGE_NONE][0]['timezone']));

  $registration_data['ConsumerEvents'] = [
    (object) [
      'ExternalEventID' => $external_event_id,
      'EventName' => incy_iqvia_cleartrak_bulid_event_title($event),
      'EventStartDate' => $event_start_date->format('m/d/Y H:i:s'),
      'EventEndDate' => $event_end_date->format('m/d/Y H:i:s'),
      'TouchPointActivity' => 'Patient Meeting Registration',
      'TrackingCode' => $trackingCode,
    ],
  ];

  return $registration_data;
}

/**
 * Returns an external customer or event id.
 */
function incy_iqvia_cleartrak_get_external_id($node_id) {
  return 'I' . $node_id;
}

/**
 * Returns a list of US states abbrs and codes.
 */
function incy_iqvia_cleartrak_state_ids_list() {
  $state_ids = array(
    'AA' => 1,
    'AE' => 2,
    'AK' => 3,
    'AL' => 4,
    'AP' => 5,
    'AR' => 6,
    'AS' => 7,
    'AZ' => 8,
    'CA' => 9,
    'CO' => 10,
    'CT' => 11,
    'DC' => 12,
    'DE' => 13,
    'FL' => 14,
    'FM' => 15,
    'GA' => 16,
    'GU' => 17,
    'HI' => 18,
    'IA' => 19,
    'ID' => 20,
    'IL' => 21,
    'IN' => 22,
    'KS' => 23,
    'KY' => 24,
    'LA' => 25,
    'MA' => 26,
    'MD' => 27,
    'ME' => 28,
    'MH' => 29,
    'MI' => 30,
    'MN' => 31,
    'MO' => 32,
    'MP' => 33,
    'MS' => 34,
    'MT' => 35,
    'NC' => 36,
    'ND' => 37,
    'NE' => 38,
    'NH' => 39,
    'NJ' => 40,
    'NM' => 41,
    'NV' => 42,
    'NY' => 43,
    'OH' => 44,
    'OK' => 45,
    'OR' => 46,
    'PA' => 47,
    'PR' => 48,
    'PW' => 49,
    'RI' => 50,
    'SC' => 51,
    'SD' => 52,
    'TN' => 53,
    'TX' => 54,
    'UT' => 55,
    'VA' => 56,
    'VI' => 57,
    'VT' => 58,
    'WA' => 59,
    'WI' => 60,
    'WV' => 61,
    'WY' => 62,
    'XX' => 63,
  );
  return $state_ids;
}

/**
 * Custom build the event title that use in the dropdown.
 *
 * @see mpnpatient_layout_build_upcoming_events_dropdown()
 */
function incy_iqvia_cleartrak_bulid_event_title($event_node) {
  $city = variable_get('mpnpatient_layout_obfuscation', 0) ? '[City]' : $event_node->field_display_city[LANGUAGE_NONE][0]['value'];
  $state = variable_get('mpnpatient_layout_obfuscation', 0) ? '[State]' : $event_node->field_event_address[LANGUAGE_NONE][0]['administrative_area'];
  $venue = variable_get('mpnpatient_layout_obfuscation', 0) ? '[Venue]' : $event_node->field_venue[LANGUAGE_NONE][0]['value'];
  $address = variable_get('mpnpatient_layout_obfuscation', 0) ? '[Address]' : $event_node->field_event_address[LANGUAGE_NONE][0]['thoroughfare'];

  $date = new DateTime($event_node->field_date[LANGUAGE_NONE][0]['value']);

  $date->setTimezone(new DateTimeZone($event_node->field_date[LANGUAGE_NONE][0]['timezone']));
  $event_day = $date->format('l, F j, Y');
  $event_start_time = $date->format('g:ia');

  $date_2 = new DateTime($event_node->field_date[LANGUAGE_NONE][0]['value2']);
  $date_2->setTimezone(new DateTimeZone($event_node->field_date[LANGUAGE_NONE][0]['timezone']));
  $event_end_time = $date_2->format('g:ia');

  $time = $event_day . ' - ' . $event_start_time . ' to ' . $event_end_time;

  return $city . ', ' . $state . ' | ' . $venue . ' - ' . $address . ' | ' . $time;
}

/**
 * Log All API Calls for future use and debugging.
 *
 * @return bool
 *   TRUE for success DB write or FALSE on fail.
 */
function incy_iqvia_cleartrak_api_write_log($node_id = NULL, $event_id = NULL, $consumer_id = NULL) {
  if (!db_table_exists('iqvia_cleartrak_api_log')) {
    watchdog('incy_iqvia_cleartrak', 'iqvia_cleartrak_api_log table does not exist');
    return FALSE;
  }
  if (empty($node_id)) {
    watchdog('incy_iqvia_cleartrak', 'registration id is empty');
    return FALSE;
  }
  $result = FALSE;
  if (is_null($consumer_id)) {
    $result = db_merge('iqvia_cleartrak_api_log')
      ->key(array('registration_node_id' => $node_id))
      ->fields(array(
        'event_node_id' => !empty($event_id) ? check_plain($event_id) : 0,
        'timestamp' => REQUEST_TIME,
      ))
      ->execute();
  }
  else {
    $hasConsumer = db_select('iqvia_cleartrak_api_log', 'n')
      ->fields('n')
      ->condition('api_consumer_id', $consumer_id)
      ->execute()
      ->fetchAssoc();

    if (!$hasConsumer || $consumer_id == 'n/a') {
      $result = db_merge('iqvia_cleartrak_api_log')
        ->key(array('registration_node_id' => $node_id))
        ->fields(array(
          'api_consumer_id' => $consumer_id,
          'timestamp' => REQUEST_TIME,
        ))
        ->execute();
    }
  }
  return $result;
}

/**
 * Obtaining IQVIA ClearTrak API token.
 */
function incy_iqvia_cleartrak_obtain_token() {

  $endpoint = rtrim(variable_get('iqvia_cleartrak_api_endpoint', ''), "/");
  $headers = [
    'Content-Type' => 'application/x-www-form-urlencoded',
  ];
  $options = [
    'grant_type' => 'password',
    'username' => variable_get('iqvia_cleartrak_api_username', ''),
    'password' => variable_get('iqvia_cleartrak_api_password', ''),
  ];
  $params = [
    'method' => 'POST',
    'data' => drupal_http_build_query($options),
    'headers' => $headers,
  ];
  $response = drupal_http_request($endpoint . "/Token", $params);
  incy_iqvia_cleartrak_error_handling($response);

  if ($response->status_message == 'OK' && !empty($response->data)) {
    if (INCY_IQVIA_CLEARTRAK_ENABLE_DEBUG_MODE) {
      watchdog('incy_iqvia_cleartrak', 'OBTAINING TOKEN REST CALL');
    }
    $response_data = json_decode($response->data);
  }

  return isset($response_data->access_token) ? $response_data->access_token : "";
}

/**
 * Error logging.
 */
function incy_iqvia_cleartrak_error_handling($response) {
  if (isset($response->error)) {
    watchdog(
      'IQVIA ClearTrak API',
      'Error @code: @error',
      [
        '@code' => $response->code,
        '@error' => $response->error,
      ],
      WATCHDOG_ERROR
    );
  }
}

/**
 * IQVIA ClearTrak Consumer Registration.
 */
function incy_iqvia_cleartrak_consumer_registration($registration_data, $token) {
  $endpoint = rtrim(variable_get('iqvia_cleartrak_api_endpoint', ''), "/");
  $headers = [
    'Authorization' => 'Bearer ' . $token,
    'Content-Type' => 'application/json',
  ];
  $params = [
    'headers' => $headers,
    'method' => 'POST',
    'data' => json_encode($registration_data),
  ];

  $response = drupal_http_request($endpoint . "/api/ConsumerRegistration", $params);
  incy_iqvia_cleartrak_error_handling($response);

  if ($response->status_message == 'OK' && !empty($response->data)) {
    if (INCY_IQVIA_CLEARTRAK_ENABLE_DEBUG_MODE) {
      watchdog('incy_iqvia_cleartrak', 'Consumer Registration REST CALL- Response <pre>@oo_response</pre>', array('@oo_response' => print_r($response, TRUE)));
    }
    return TRUE;
  }
  return FALSE;
}

/**
 * Update Consumer OptOut status.
 */
function incy_iqvia_cleartrak_update_consumer_optout() {
  $event_registrations = views_get_view_result('iqvia_cleartrak_registration_list', 'optouts');
  foreach ($event_registrations as $key => $registration) {
    $registration_node = $registration->_field_data['nid']['entity'];

    if (!empty($registration_node->field_iqvia_api_sent[LANGUAGE_NONE][0]['value']) &&
      is_null(incy_iqvia_cleartrak_get_consumer_id($registration_node->nid))) {

      if (incy_iqvia_cleartrak_check_optout($registration_node) === FALSE) {
        $registration_node->field_optout[LANGUAGE_NONE][0]['value'] = 'email';
      }
      else {
        $registration_node->field_optout[LANGUAGE_NONE][0]['value'] = NULL;
      }

      field_attach_update('node', $registration_node);

      if (INCY_IQVIA_CLEARTRAK_ENABLE_DEBUG_MODE) {
        watchdog('incy_iqvia_cleartrak_OptOut', 'Registration OptOut updated @nid', array('@nid' => $registration_node->nid));
      }
    }
  }
}

/**
 * Gets Consumer id by registration id.
 */
function incy_iqvia_cleartrak_get_consumer_id($registration_id) {
  $consumer_id = db_select('iqvia_cleartrak_api_log', 'n')
    ->fields('n', array('api_consumer_id'))
    ->condition('registration_node_id', $registration_id)
    ->execute()
    ->fetchField();
  return $consumer_id;
}

/**
 * Check Email optout.
 *
 * @return bool
 *   returns false if user is optout
 */
function incy_iqvia_cleartrak_check_optout($registration_node) {
  $ret = TRUE;

  $token = incy_iqvia_cleartrak_obtain_token();

  $topic_ids = incy_iqvia_cleartrak_get_topic_ids();

  $optouts = incy_iqvia_cleartrak_optout_api_request($registration_node, $token);

  if (!empty($optouts)) {
    foreach ($optouts as $key => $optout) {
      if (in_array($optout->CategoryID, $topic_ids)) {
        if($optout->OptOutFlag == 1) {
          $ret = FALSE;
        }
      }
    }
  }

  return $ret;
}

/**
 * Get Optout for user using consumer id.
 */
function incy_iqvia_cleartrak_optout_api_request($registration_node = NULL, $token = NULL) {
  if (empty($registration_node) || empty($token)) {
    return FALSE;
  }

  $event_id = $registration_node->field_event[LANGUAGE_NONE][0]['target_id'];
  $external_event_id = incy_iqvia_cleartrak_get_external_id($event_id);

  $endpoint = rtrim(variable_get('iqvia_cleartrak_api_endpoint', ''), "/") . "/api/ConsumerOptExport";
  if (false !== $consumer_id = incy_iqvia_cleartrak_get_registration_consumer_id($registration_node)) {
    $query_params = "?" . drupal_http_build_query(['ConsumerID' => $consumer_id]);
  }
  else {
    $query_params = "?" . drupal_http_build_query(['ExternalEventID' => $external_event_id]);
  }
  $headers = [
    'Authorization' => 'Bearer ' . $token,
    'Content-Type' => 'application/json',
  ];
  $params = [
    'headers' => $headers,
    'method' => 'GET',
  ];
  $response = drupal_http_request($endpoint . $query_params, $params);
  incy_iqvia_cleartrak_error_handling($response);

  if ($response->status_message == 'OK' && !empty($response->data)) {
    if (INCY_IQVIA_CLEARTRAK_ENABLE_DEBUG_MODE) {
      watchdog('incy_iqvia_cleartrak', 'OPTOUT REST CALL- Response <pre>@oo_response</pre>', array('@oo_response' => print_r($response, TRUE)));
    }
    $response_data = json_decode($response->data);
    $response_data_obj = incy_iqvia_cleartrak_filter_optouts($registration_node, $response_data);

    $consumer_id = isset($response_data_obj->ConsumerID) ? $response_data_obj->ConsumerID : 'n/a';
    $log_api_calls = incy_iqvia_cleartrak_api_write_log($registration_node->nid, NULL, $consumer_id);

    $registration_node->field_iqvia_api_contact_id[LANGUAGE_NONE][0]['value'] = $consumer_id;

    field_attach_update('node', $registration_node);

    return isset($response_data_obj->ConsumerOptDetails) ? $response_data_obj->ConsumerOptDetails : [];
  }

  return FALSE;
}

/**
 * Get opt-out topic ids.
 *
 * JAK Consumer PV Branded EM,
 * JAK Consumer MF Branded EM,
 * JAK Consumer IncyteCARES PV EM,
 * JAK Consumer IncyteCARES MF EM,
 * JAK Consumer Market Research EM.
 */
function incy_iqvia_cleartrak_get_topic_ids() {
  if (incy_iqvia_cleartrak_is_test_api()) {
    return [
      '127520F3-AE53-4C5C-9DBD-FAB7D73D303B',
      'D88F3853-C2D2-4A87-B818-B8BBB9BF3AC6',
      'B5EC6530-B4D5-4923-A550-EC7974F7F1B2',
      'B1F5840F-A939-48F4-80B4-22CBFDB78D4D',
      '08EA018C-1B32-4058-8FEC-210DB999EF09',
    ];
  }
  return [
    '77E0577F-A05D-4741-A960-1D7BEBDC102F',
    '8A4A5A2E-0E6F-4A90-A47C-CC5AA0D1D93B',
    'AA306CCC-5298-45F2-A6F4-AB8C98A134F0',
    '3D17F8F7-BF8C-4B06-A75D-20BD940A1146',
    'CA270A89-BAA8-426F-8941-C6E7AC626D32',
  ];
}

/**
 * Get tracking code.
 */
function incy_iqvia_cleartrak_get_tracking_code() {
  return incy_iqvia_cleartrak_is_test_api() ? '813D9FA5' : '19ED254E';
}

/**
 * Defines if test api is using.
 */
function incy_iqvia_cleartrak_is_test_api() {
  $api_url = variable_get('iqvia_cleartrak_api_endpoint', 'https://incyte-cleartrak.solutions.iqvia.com');
  if (false !== strpos($api_url, 'test')) {
    return true;
  }
  return false;
}

/**
 * Filter Opt-outs response
 */
function incy_iqvia_cleartrak_filter_optouts($registration_node, $optouts) {
  if (false !== incy_iqvia_cleartrak_get_registration_consumer_id($registration_node)) {
    $optout = reset($optouts);
    return $optout;
  }
  $firstName = isset($registration_node->field_first_name[LANGUAGE_NONE][0]['value']) ? $registration_node->field_first_name[LANGUAGE_NONE][0]['value'] : '';
  $lastName = isset($registration_node->field_last_name[LANGUAGE_NONE][0]['value']) ? $registration_node->field_last_name[LANGUAGE_NONE][0]['value'] : '';
  if (!empty($optouts)) {
    foreach ($optouts as $optout) {
      if($optout->FirstName == $firstName && $optout->LastName == $lastName) {
        return $optout;
      }
    }
  }
  return [];
}

/**
 * Returns IQVIA consumer id or false.
 */
function incy_iqvia_cleartrak_get_registration_consumer_id($registration_node) {
  if (!empty($registration_node->field_iqvia_api_contact_id[LANGUAGE_NONE][0]['value'])) {
    return $registration_node->field_iqvia_api_contact_id[LANGUAGE_NONE][0]['value'];
  }
  return false;
}
