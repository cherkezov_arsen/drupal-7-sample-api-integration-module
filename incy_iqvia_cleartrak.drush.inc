<?php

/**
 * @file
 * IQVIA ClearTrak Drush Commands.
 */

/**
 * Implements hook_drush_command().
 */
function incy_iqvia_cleartrak_drush_command() {

  $items['incy_iqvia_cleartrak_push_all_reg'] = array(
    'description' => 'Push all Webinars and Live Lessons registrations.',
    'aliases' => array('pawr'),
    'callback' => 'incy_iqvia_cleartrak_push_all_registrations_to_iqvia_api',
    'options' => array(),
  );

  $items['incy_iqvia_cleartrak_update_optouts'] = array(
    'description' => 'Update Registration optout fields values',
    'aliases' => array('pawr'),
    'callback' => 'incy_iqvia_cleartrak_update_consumer_optout',
    'options' => array(),
  );

  return $items;
}
